#include "Poisson.h"

Poisson::Poisson(int id, std::string nom, float longueur) : Animal(id, nom, TypeAnimal::POISSON), longueur(longueur)
{
}

void Poisson::afficher() const
{
	Animal::afficher();
	std::cout << "Longueur : " << this->longueur << "cm" << std::endl;
}

void Poisson::nager() const
{
	std::cout << this->getNom() << " le poisson nage !" << std::endl;
}
