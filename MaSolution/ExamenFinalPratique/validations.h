#pragma once

class Valider {
public: 
	// int 
	static void vInt(int &aValider);
	static void vIntEntreBorne(int &aValider, int min, int max);

	// float
	static void vFloat(float &aValider);
	static void vFloatPositif(float &aValider);
};