#include "Application.h"
#include <conio.h>
#include <iostream>

Animalerie Application::animalerie;

/*
	Il est interdit de modifier le code source 
	de cette m�thode (afficherMenu()).
*/
void Application::afficherMenu() {
	char choix;
	do {
		system("cls");
		std::cout << "Gestionnaire d'animalerie" << std::endl;
		std::cout << "Voici les options qui vous sont disponibles : " << std::endl;
		std::cout << " (A)jouter un animal" << std::endl;
		std::cout << " (V)endre un animal" << std::endl;
		std::cout << " Afficher (t)ous les animaux vendus" << std::endl;
		std::cout << " (F)aire bouger les animaux" << std::endl;
		std::cout << " (Q)uitter" << std::endl;
		std::cout << "Votre choix : ";
		choix = toupper(_getch());
		system("cls");
		if (choix != 'Q') {
			switch (choix) {
			case 'A':
				Application::ajouterUnAnimal();
				system("pause");
				break;
			case 'V':
				Application::vendreUnAnimal();
				system("pause");
				break;
			case 'T':
				Application::afficherLesAnimauxVendus();
				system("pause");
				break;
			case 'F':
				Application::faireBougerLesAnimaux();
				system("pause");
				break;
			}

		}
	} while (choix != 'Q');
}

void Application::ajouterUnAnimal() {
	animalerie.ajouterAnimal();
}

void Application::vendreUnAnimal() {
	animalerie.vendreAnimal();
}

void Application::afficherLesAnimauxVendus() {
	animalerie.afficherTousLesAnimauxVendus();
}

void Application::faireBougerLesAnimaux() {
	animalerie.faireBougerAnimaux();
}