#pragma once
#include "Animal.h"

class Oiseau : public Animal
{

public: 
	Oiseau(int id, std::string nom);
	void voler() const;
};