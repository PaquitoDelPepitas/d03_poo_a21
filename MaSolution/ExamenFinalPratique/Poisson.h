#pragma once
#include "Animal.h"

class Poisson : public Animal
{
	float longueur;

public:
	Poisson(int id, std::string nom, float longueur);
	void afficher() const override;
	void nager() const;
};