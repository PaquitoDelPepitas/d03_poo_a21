#pragma once
#include "Animal.h"

class Animalerie
{
	static const unsigned short TAILLE_ANIMALERIE = 10;
	Animal* tabAnimal[TAILLE_ANIMALERIE];
	unsigned short nbAnimal;

	int chercherAnimalDisponible(int idSaisi) const;

public:
	Animalerie(); // Le seul constructeur demand�. Ne pas en ajouter d'autre.

	void ajouterAnimal();
	void vendreAnimal();
	void afficherTousLesAnimauxVendus() const;
	void faireBougerAnimaux() const;
};