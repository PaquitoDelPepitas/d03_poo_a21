#include "Animalerie.h"
#include <iostream>
#include <string>
#include "validations.h"
#include "Oiseau.h"
#include "Poisson.h"

Animalerie::Animalerie() {
	this->nbAnimal = 0;
}

int Animalerie::chercherAnimalDisponible(int idSaisi) const
{
	int index = -1, cpt = 0;

	while (index == -1 && cpt < this->nbAnimal) {
		if (idSaisi == this->tabAnimal[cpt]->getId()) {
			index = cpt;
		}
		cpt++;
	}

	return index;
}

void Animalerie::ajouterAnimal()
{
	if (this->nbAnimal < this->TAILLE_ANIMALERIE) {
		Animal* newAnimal;

		std::cout << "Veuillez renseigner les informations suivantes pour ajouter l'animal dans l'animalerie : " << std::endl;

		int id;
		std::cout << "Identifiant --> ";
		Valider::vInt(id);
		while (this->chercherAnimalDisponible(id) != -1) {
			std::cout << "Cet identifiant est deja attribu� a l'un de nos amis, veuillez en saisir un autre ";
			std::cout << "--> ";
			Valider::vInt(id);
		}

		std::string nom;
		std::cout << "Nom --> ";
		std::getline(std::cin, nom);

		int type;
		std::cout << "Quel animal est " << nom << " ?" << std::endl;
		std::cout << "(" << (int)TypeAnimal::OISEAU << ") - Un oiseau" << std::endl;
		std::cout << "(" << (int)TypeAnimal::POISSON << ") - Un poisson" << std::endl;
		std::cout << "--> ";
		Valider::vIntEntreBorne(type, 0, 1);

		if (type == (int)TypeAnimal::POISSON) {
			float longueur;
			std::cout << "Longueur (en cm) --> ";
			Valider::vFloatPositif(longueur);

			newAnimal = new Poisson(id, nom, longueur);
		}
		else {
			newAnimal = new Oiseau(id, nom);
		}

		this->tabAnimal[this->nbAnimal] = newAnimal;
		nbAnimal++;
	}
	else {
		std::cout << "Notre systeme ne peut contenir plus d'animaux, nous nous excusons pour la g�ne occasionn�..." << std::endl;
	}
}

void Animalerie::vendreAnimal()
{
	int id;
	std::cout << "Veuillez saisir l'identifiant (id) de l'animal que vous voudriez vendre" << std::endl;
	std::cout << "--> ";
	Valider::vInt(id);

	int animal = this->chercherAnimalDisponible(id);

	if (animal == -1) {
		std::cout << "Aucun animal ne correspond a l'identifiant saisi (" << id << ")" << std::endl;
	}
	else if (this->tabAnimal[animal]->getVendu()) {
		std::cout << "Cet animal a d�ja trouv� refuge aupr�s d'une famille." << std::endl;
	}
	else {
		this->tabAnimal[animal]->setVendu();
		std::cout << "L'animal suivant a d�sormais trouv� une famille !" << std::endl;
		this->tabAnimal[animal]->afficher();
	}
}

void Animalerie::afficherTousLesAnimauxVendus() const
{
	short animalVendu = 0;

	for (int i = 0; i < this->nbAnimal; i++) {
		if (this->tabAnimal[i]->getVendu()) {
			this->tabAnimal[i]->afficher();
			std::cout <<std::endl;
			animalVendu++;
		}
	}

	if (!animalVendu) {
		std::cout << "Malheuresement, aucun de nos amis n'a encore trouv� refuge... Mais ca va venir tr�s bientot !" << std::endl;
	}
}

void Animalerie::faireBougerAnimaux() const
{
	if (this->nbAnimal > 0) {

		int animalVendu = 0;

		for (int i = 0; i < this->nbAnimal; i++) {
			if (!this->tabAnimal[i]->getVendu()) {
				TypeAnimal type = this->tabAnimal[i]->getType();
				switch (type) {
				case TypeAnimal::OISEAU:
					((Oiseau*)this->tabAnimal[i])->voler();
					break;
				case TypeAnimal::POISSON:
					((Poisson*)this->tabAnimal[i])->nager();
					break;
				default:
					break;
				}
			}
			else {
				animalVendu++;
			}
		}

		if (animalVendu == this->nbAnimal) {
			std::cout << "Tous nos amis ont trouv� refuge !" << std::endl;
		}
	}
	else {
		std::cout << "Nous n'avons encore receuilli aucun animal dans notre animalerie..." << std::endl;
	}
}
