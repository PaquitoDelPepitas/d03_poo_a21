#pragma once

#include <string>
#include <iostream>
#include "TypeAnimal.h"

class Animal {

	int id;
	std::string nom;
	TypeAnimal type;
	bool vendu;

public:
	Animal(int id, std::string nom, TypeAnimal type);

	int getId() const;
	std::string getNom() const;
	TypeAnimal getType() const;
	bool getVendu() const;

	void setVendu();

	virtual void afficher() const;

	virtual ~Animal();
};