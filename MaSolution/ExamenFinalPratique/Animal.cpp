#include "Animal.h"

Animal::Animal(int id, std::string nom, TypeAnimal type) : id(id), nom(nom), type(type)
{
	this->vendu = false;
}

int Animal::getId() const
{
	return this->id;
}

std::string Animal::getNom() const
{
	return this->nom;
}

TypeAnimal Animal::getType() const
{
	return this->type;
}

bool Animal::getVendu() const
{
	return this->vendu;
}

void Animal::setVendu()
{
	this->vendu = !this->vendu;
}

void Animal::afficher() const
{
	std::cout << "Nom : " << this->getNom() << std::endl;
	std::cout << "Animal : " << ((int)this->getType() ? "Poisson" : "Oiseau") << std::endl;
	std::cout << "Identifiant : " << this->getId() << std::endl;
	std::cout << "Etat : " << (this->getVendu() ? "Adopt�.e" : "En attente d'une famille") << std::endl;
}

Animal::~Animal()
{
	delete this;
}
