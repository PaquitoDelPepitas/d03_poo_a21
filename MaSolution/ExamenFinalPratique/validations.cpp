#include "validations.h"
#include <iostream>

// int
void Valider::vInt(int & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
void Valider::vIntEntreBorne(int & aValider, int min, int max)
{
	Valider::vInt(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::vInt(aValider);
	}
}

// float
void Valider::vFloat(float & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
void Valider::vFloatPositif(float & aValider)
{
	Valider::vFloat(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::vFloat(aValider);
	}
}