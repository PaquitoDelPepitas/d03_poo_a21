#include "Oiseau.h"

Oiseau::Oiseau(int id, std::string nom) : Animal(id, nom, TypeAnimal::OISEAU)
{
}

void Oiseau::voler() const
{
	std::cout << this->getNom() << " l'oiseau vole !" << std::endl;
}
