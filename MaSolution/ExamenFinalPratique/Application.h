#pragma once
#include "Animalerie.h"
class Application
{
	static Animalerie animalerie; //Donnée membre static (de classe) de type Animalerie
	static void ajouterUnAnimal();
	static void vendreUnAnimal();
	static void afficherLesAnimauxVendus();
	static void faireBougerLesAnimaux();
public:		
	static void afficherMenu();
};

