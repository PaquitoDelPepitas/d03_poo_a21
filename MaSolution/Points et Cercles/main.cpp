#include <iostream>
using namespace std;

#include "Point2D.h"
#include "CercleSimple.h"
#include "CercleCompose.h"

int main() {
	setlocale(LC_ALL, "");

	Point2D* point = new Point2D(1, 3);
	cout << "Point 2D seul : ";
	point->afficher();

	cout << endl;

	CercleSimple* cercleSimple = new CercleSimple(1, 3, 10);
	cout << "Cercle simple : ";
	cercleSimple->afficher();

	cout << endl;

	CercleCompose* cercleCompose = new CercleCompose(*point, 10);
	cout << "Cercle compos� : ";
	cercleCompose->afficher();

	delete point;
	delete cercleSimple;
	delete cercleCompose;

	system("pause");
	return 0;
}