#include "CercleCompose.h"
#include <iostream>

CercleCompose::CercleCompose(Point2D coord, int rayon) : coord(coord)
{
	this->rayon = rayon;
}

void CercleCompose::afficher() const
{
	std::cout << "Cercle de rayon " << this->rayon << " et de centre ";
	coord.afficher();
	std::cout << std::endl;
}
