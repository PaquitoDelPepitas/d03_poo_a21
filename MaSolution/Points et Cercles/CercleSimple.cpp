#include "CercleSimple.h"
#include <iostream>



CercleSimple::CercleSimple(int x, int y, int rayon)
{
	this->x = x;
	this->y = y;
	this->rayon = rayon;
}

void CercleSimple::afficher() const
{
	std::cout << "Cercle de rayon " << this->rayon << " et de centre (" << this->x << ";" << this->y << ")" << std::endl;
}
