#include "Point2D.h"
#include <iostream>


Point2D::Point2D(int x, int y)
{
	this->x = x;
	this->y = y;
}

void Point2D::afficher() const
{
	std::cout << "(" << this->x << ";" << this->y << ")" << std::endl;
}
