#pragma once
#include "Point2D.h"

class CercleCompose
{
	Point2D coord;
	int rayon;

public:
	CercleCompose(Point2D coord, int rayon);
	void afficher() const;
};

