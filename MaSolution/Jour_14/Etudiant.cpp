#include "Etudiant.h"
#include <iostream>

Etudiant::Etudiant()
{
	this->nom = "N/D";
	this->noDossier = 0;
	this->nbNotes = 0;
}

Etudiant::Etudiant(std::string nom, unsigned int noDossier, unsigned short note[], unsigned short nbNotes) : nom(nom), noDossier(noDossier), nbNotes(nbNotes)
{
	for (int i = 0; i < nbNotes; i++) {
		this->note[i] = note[i];
	}
}

void Etudiant::afficherEtudiant() const
{
	std::cout << "Nom : " << this->nom << std::endl;
	std::cout << "Num�ro de dossier : " << this->noDossier << std::endl;
	std::cout << "Notes : ";
	for (int i = 0; i < this->nbNotes; i++) {
		std::cout << this->note[i] << " ";
	}
	std::cout << std::endl;
}

Etudiant * Etudiant::lireEtudiant()
{
	Etudiant* etudiant = new Etudiant();

	std::cout << "Nom de l'�tudiant --> ";
	std::cin.ignore(512, '\n');
	std::getline(std::cin, etudiant->nom);

	std::cout << "Num�ro de dossier --> ";
	std::cin >> etudiant->noDossier;

	std::cout << "Nombre de note(s) --> ";
	std::cin >> etudiant->nbNotes;

	for (int i = 0; i < etudiant->nbNotes; i++) {
		std::cout << "Note " << i + 1 << " --> ";
		std::cin >> etudiant->note[i];
	}

	std::cout << std::endl;

	return etudiant;
}
