#pragma once
#include <string>
class Etudiant
{
	std::string nom;

	static const  unsigned short NOTE_MIN = 0, NOTE_MAX = 100;
	unsigned short note[100];
	unsigned short nbNotes;

public:
	unsigned int noDossier;

	Etudiant();
	Etudiant(std::string nom, unsigned int noDossier, unsigned short note[], unsigned short nbNotes);

	void afficherEtudiant() const;

	static Etudiant* lireEtudiant();
};

