#pragma once
#include "Etudiant.h"
class Groupe
{
	unsigned int nbEtudiants;
	Etudiant etudiant[100];

public:
	Groupe();
	static Groupe* lireGroupe();
	void afficherGroupe() const;
	static void echangerEtudiant(Groupe* groupe, int etu1, int etu2);
	static void trierNoDossier(Groupe* groupe);
};

