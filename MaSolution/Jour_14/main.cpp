#include <iostream>
using namespace std;

#include "Etudiant.h"
#include "Groupe.h"

int main() {
	setlocale(LC_ALL, "");

	Groupe* groupe = Groupe::lireGroupe();
	groupe->afficherGroupe();
	groupe->echangerEtudiant(groupe, 1, 3);
	groupe->afficherGroupe();
	groupe->trierNoDossier(groupe);
	groupe->afficherGroupe();
	delete groupe;

	system("pause");
	return 0;
}