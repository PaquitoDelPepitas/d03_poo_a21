#include "Groupe.h"
#include <iostream>
#include "Etudiant.h"


Groupe::Groupe()
{
	this->nbEtudiants = 0;
}

Groupe * Groupe::lireGroupe()
{
	Groupe* groupe = new Groupe();

	std::cout << "Nombre d'�tudiant(s) dans ce groupe --> ";
	std::cin >> groupe->nbEtudiants;
	std::cout << std::endl;

	for (int i = 0; i < groupe->nbEtudiants; i++) {
		std::cout << "Etudiant " << i + 1 << std::endl;
		groupe->etudiant[i] = *Etudiant::lireEtudiant();
	}

	return groupe;
}

void Groupe::afficherGroupe() const
{
	std::cout << "Nombre d'�tudiant(s) dans ce groupe : " << this->nbEtudiants << std::endl;
	std::cout << std::endl;

	for (int i = 0; i < this->nbEtudiants; i++) {
		std::cout << "Etudiant " << i + 1 << std::endl;
		this->etudiant[i].afficherEtudiant();
		std::cout << std::endl;
	}
}

void Groupe::echangerEtudiant(Groupe* groupe, int etu1, int etu2)
{
	Etudiant etudiantSave = groupe->etudiant[etu1 - 1];
	groupe->etudiant[etu1 - 1] = groupe->etudiant[etu2 - 1];
	groupe->etudiant[etu2 - 1] = etudiantSave;
}

void Groupe::trierNoDossier(Groupe * groupe)
{
	bool permut;
	int iteration = groupe->nbEtudiants - 1;

	do{
		permut = false;

		for (int i = 0; i < iteration; i++) {
			if (groupe->etudiant[i].noDossier > groupe->etudiant[i + 1].noDossier) {
				Etudiant etudiantSave = groupe->etudiant[i];
				groupe->etudiant[i] = groupe->etudiant[i + 1];
				groupe->etudiant[i + 1] = etudiantSave;
				permut = true;
			}
		}

		iteration--;
	} while (permut);
}
