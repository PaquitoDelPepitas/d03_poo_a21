#include <iostream>
#include "Recipient.h"

using namespace std;

int main() {
	setlocale(LC_ALL, "");

	Recipient* vase3 = new Recipient(3);
	//vase3->init(3);

	Recipient* vase5 = new Recipient(5);
	//vase5->init(5);

	vase5->remplir();
	cout << vase3->getVolume() << " // " << vase5->getVolume() << endl;
	vase5->transvaser(*vase3);
	cout << vase3->getVolume() << " // " << vase5->getVolume() << endl;
	vase3->vider();
	cout << vase3->getVolume() << " // " << vase5->getVolume() << endl;
	vase5->transvaser(*vase3);
	cout << vase3->getVolume() << " // " << vase5->getVolume() << endl;

	delete vase3;
	delete vase5;

	cout << endl;

	Recipient* vase3L = new Recipient(3);
	//vase3L->init(3);

	Recipient* vase10 = new Recipient(10);
	//vase10->init(10);

	vase10->remplir();
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;
	vase10->transvaser(*vase3L);
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;
	vase3L->vider();
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;
	vase10->transvaser(*vase3L);
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;
	vase3L->vider();
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;
	vase10->transvaser(*vase3L);
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;
	vase3L->vider();
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;
	vase10->transvaser(*vase3L);
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;
	vase10->remplir();
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;
	vase10->transvaser(*vase3L);
	cout << vase3L->getVolume() << " // " << vase10->getVolume() << endl;



	delete vase3L;
	delete vase10;

	system("pause");
	return 0;
}