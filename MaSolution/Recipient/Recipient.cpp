#include "Recipient.h"
/* 
void Recipient::init(double uneCapacite)
{
	Recipient::capacite = uneCapacite;
	Recipient::volume = 0;
}
*/


Recipient::Recipient(double capacite)
{
	Recipient::capacite = capacite;
	Recipient::volume = 0;
}

double Recipient::getCapacite()
{
	return Recipient::capacite;
}

double Recipient::getVolume()
{
	return Recipient::volume;
}

void Recipient::vider()
{
	Recipient::volume = 0;
}

void Recipient::remplir()
{
	Recipient::volume = Recipient::capacite;
}

void Recipient::transvaser(Recipient & dest)
{
	if(Recipient::volume > dest.capacite){
		Recipient::volume -= dest.capacite - dest.volume;
		dest.volume = dest.capacite;
	}
	else if (Recipient::volume < dest.capacite) {
		double save = Recipient::volume;
		Recipient::volume -= (dest.capacite - dest.volume >= Recipient::capacite ? Recipient::volume : dest.capacite - dest.volume);
		Recipient::volume = (Recipient::volume < 0 ? 0 : Recipient::volume);
		dest.volume += (dest.volume + Recipient::volume > dest.capacite ? dest.capacite : save);
	}
}
