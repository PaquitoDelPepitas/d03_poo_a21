#pragma once
class  Recipient {

public:
	double 	capacite, volume;

	Recipient(double capacite);

	// void init(double uneCapacite);

	double getCapacite();
	double getVolume();	

	void vider();
	void remplir();
	void transvaser(Recipient &dest);
};

