#pragma once
#include "Document.h"

class Multimedia : public Document
{

protected:
	unsigned int duree;

public:
	Multimedia();
	Multimedia(std::string titre, int noReference, unsigned int duree);

	void afficher() const;

	~Multimedia();
};

