#include "Document.h"
#include <iostream>
#include <string>

unsigned short nbDoc = 0;

Document::Document()
{
	this->titre = "N/D";
	this->noReference = 0;
	this->emprunt = false;
}

Document::Document(std::string titre, int noReference) : titre(titre), noReference(noReference) 
{
	this->emprunt = false;
}

void Document::afficher() const
{
	std::cout << "Information du document" << std::endl;
	std::cout << "Titre : " << this->titre << std::endl;
	std::cout << "No de r�f�rence : " << this->noReference << std::endl;
	std::cout << "Etat : " << (this->emprunt ? "Emprunt�" : "Disponible") << std::endl;
}

void Document::emprunter()
{
	this->emprunt = (this->emprunt ? false : true);
	std::cout << "Ce document est maintenant " << (this->emprunt ? "emprunt�" : "disponible") << std::endl;
	std::cout << std::endl;
}

TypeDoc Document::getType() const
{
	return this->type;
}


Document::~Document()
{
}
