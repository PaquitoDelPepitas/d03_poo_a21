#pragma once
#include "Document.h"

class Livre : public Document
{
	unsigned int nbPage;

public:
	Livre();
	Livre(std::string titre, int noReference, unsigned int nbPage);

	void afficher() const;

	~Livre();
};

