#include <iostream>
using namespace std;

#include "Document.h"
#include "Livre.h"
#include "Multimedia.h"
#include "CD.h"
#include "DVD.h"

int main() {
	setlocale(LC_ALL, "");

	DVD* doc1 = new DVD("Pepito", 123, 50, true);
	CD* doc2 = new CD("Vroum vroum", 456, 78, false);
	Livre* doc3 = new Livre("Livre de la morkitu", 789, 850);

	Document* tabDoc[3] = { doc1, doc2, doc3 };

	for (int i = 0; i < 3; i++) {
		if (tabDoc[i]->getType() == TypeDoc::LIVRE) {
			((Livre*)tabDoc[i])->afficher();
		}
		else if (tabDoc[i]->getType() == TypeDoc::DVD) {
			((DVD*)tabDoc[i])->afficher();
		}
		else if (tabDoc[i]->getType() == TypeDoc::CD) {
			tabDoc[i]->emprunter();
			((CD*)tabDoc[i])->afficher();
		}
	}

	delete doc1, doc2, doc3;

	system("pause");
	return 0;
}