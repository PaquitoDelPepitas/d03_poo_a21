#include "DVD.h"
#include <iostream>


DVD::DVD() : Multimedia()
{
	this->bonus = false;
	this->type = TypeDoc::DVD;
}

DVD::DVD(std::string titre, int noReference, unsigned int duree, bool bonus) : Multimedia(titre, noReference, duree), bonus(bonus)
{
	this->type = TypeDoc::DVD;
}

void DVD::afficher() const
{
	Multimedia::afficher();
	std::cout << "Bonus : " << (this->bonus ? "Oui" : "Non") << std::endl;
	std::cout << std::endl;
}


DVD::~DVD()
{
}
