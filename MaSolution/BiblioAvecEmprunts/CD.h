#pragma once
#include "Multimedia.h"

class CD : public Multimedia
{
	unsigned int nbPlage;

public:
	CD();
	CD(std::string titre, int noReference, unsigned int duree, unsigned int nbPlage);

	void afficher() const;

	~CD();
};


