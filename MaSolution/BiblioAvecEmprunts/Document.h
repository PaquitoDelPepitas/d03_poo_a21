#pragma once
#include <string>
#include "TypeDoc.h"

class Document
{

protected:
	std::string titre;
	int noReference;
	bool emprunt;

	TypeDoc type;

public:
	Document();
	Document(std::string titre, int noReference);

	void afficher() const;
	void emprunter();

	TypeDoc getType() const;

	~Document();
};

