#include "CD.h"
#include <iostream>


CD::CD() : Multimedia()
{
	this->nbPlage = 0;
	this->type = TypeDoc::CD;
}

CD::CD(std::string titre, int noReference, unsigned int duree, unsigned int nbPlage) : Multimedia(titre, noReference, duree), nbPlage(nbPlage)
{
	this->type = TypeDoc::CD;
}

void CD::afficher() const
{
	Multimedia::afficher();
	std::cout << "Nombre de plages : " << this->nbPlage << std::endl;
	std::cout << std::endl;
}


CD::~CD()
{
}
