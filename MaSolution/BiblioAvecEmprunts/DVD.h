#pragma once
#include "Multimedia.h"

class DVD : public Multimedia
{
	bool bonus;

public:
	DVD();
	DVD(std::string titre, int noReference, unsigned int duree, bool bonus);

	void afficher() const;

	~DVD();
};

