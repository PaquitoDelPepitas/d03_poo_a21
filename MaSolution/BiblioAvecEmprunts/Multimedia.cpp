#include "Multimedia.h"
#include <iostream>


Multimedia::Multimedia() :  Document()
{
	this->duree = 0;
}

Multimedia::Multimedia(std::string titre, int noReference, unsigned int duree) : Document(titre, noReference), duree(duree)
{
}

void Multimedia::afficher() const
{
	Document::afficher();
	std::cout << "Dur�e : " << this->duree << " min" << std::endl;
}


Multimedia::~Multimedia()
{
}
