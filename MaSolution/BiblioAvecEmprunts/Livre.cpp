#include "Livre.h"
#include <iostream>

Livre::Livre() : Document()
{
	this->nbPage = 0;
	this->type = TypeDoc::LIVRE;
}

Livre::Livre(std::string titre, int noReference, unsigned int nbPage) : Document(titre, noReference), nbPage(nbPage)
{	
	this->type = TypeDoc::LIVRE;
}

void Livre::afficher() const
{
	Document::afficher();
	std::cout << "Nombre de pages : " << this->nbPage << std::endl;
	std::cout << std::endl;
}


Livre::~Livre()
{
}
