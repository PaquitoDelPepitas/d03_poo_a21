#pragma once
#include "Personne.h"
#include "TypeCompte.h"

class Compte
{
	int noCompte;
	Personne titulaire;
	double tauxInteret;
	TypeCompte type;

protected:
	int solde;

public:
	bool etat;

	Compte(int noCompte, Personne titulaire, int solde, double tauxInteret, TypeCompte type);

	void setNoCompte(int noCompte);
	int getNoCompte() const;

	void setTitulaire(Personne titulaire);
	Personne getTitulaire() const;

	void setSolde(int solde);
	int getSolde() const;

	void setTauxInteret(double tauxInteret);
	virtual double getTauxInteret() const;

	TypeCompte getType() const;

	virtual void afficher() const;
	
	virtual void retrait(int montant);
	void depot(int montant);
};