#include "CompteCheque.h"
#include <iostream>


CompteCheque::CompteCheque(int noCompte, Personne titulaire, int solde, double tauxInteret) :
Compte(noCompte, titulaire, solde, tauxInteret, TypeCompte::CHEQUE){
}

double CompteCheque::getTauxInteret() const
{
	return Compte::getTauxInteret() / 10;
}

void CompteCheque::afficher() const
{
	Compte::afficher();
	std::cout << "Type de compte : Ch�que" << std::endl;
}
