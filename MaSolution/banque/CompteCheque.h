#pragma once
#include "Compte.h"
class CompteCheque : public Compte
{
public:
	CompteCheque(int noCompte, Personne titulaire, int solde, double tauxInteret);
	double getTauxInteret() const override;
	void afficher() const override;
};

