#include <iostream>
using namespace std;

#include <string>

#include "Personne.h"
#include "Compte.h"
#include "Banque.h"

int main() {
	setlocale(LC_ALL, "");

	Banque* b = new Banque();
	b->ouvrirUnCompte(b->comptes, b->nbCompte);
	cout << endl;
	b->comptes[b->nbCompte - 1]->afficher();
	b->comptes[b->nbCompte - 1]->retrait(100);
	delete b;

	cout << endl;

	system("pause");
	return 0;
}