#pragma once
#include "Compte.h"
class CompteEpargne : public Compte
{

public:
	double frais;

	CompteEpargne(int noCompte, Personne titulaire, int solde, double tauxInteret);

	double getFrais() const;
	double getTauxInteret() const override;

	void retrait(int montant) override;

	void afficher() const override;
};

