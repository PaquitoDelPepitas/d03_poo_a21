#include "Banque.h"
#include <iostream>
#include <string>


Banque::Banque() : nom("N/D"), directeur("N/D"), nbCompte(0)
{
}

void Banque::ouvrirUnCompte(Compte* comptes[], unsigned int & nbCompte)
{
	std::cout << "Veuillez renseigner les informations suivante pour ouvrir votre compte" << std::endl;

	std::cout << "Votre nom --> ";
	std::string nom;
	std::getline(std::cin, nom);

	std::cout << "Le montant de votre d�pot initial --> ";
	int montantInitial;
	Valider::IntPositif(montantInitial);

	std::cout << "Le taux d'interet d�sir� (Parce qu'on est une banque genereuse) --> ";
	double tauxInteret;
	Valider::DoubleEntreBorne(tauxInteret, 0, 1);

	std::cout << "Quel type de compte voulez-vous cr�er ?" << std::endl;
	std::cout << "(" << (int)TypeCompte::CHEQUE << ") Compte Ch�que" << std::endl;
	std::cout << "(" << (int)TypeCompte::EPARGNE << ") Compte Epargne" << std::endl;
	std::cout << "--> ";
	int choix;
	Valider::IntEntreBorne(choix, (int)TypeCompte::CHEQUE, (int)TypeCompte::EPARGNE);

	switch (choix)
	{
	case (int)TypeCompte::CHEQUE:
		this->comptes[nbCompte] = new CompteCheque(nbCompte, Personne(nom), montantInitial, tauxInteret);
		break;
	case (int)TypeCompte::EPARGNE:
		this->comptes[nbCompte] = new CompteEpargne(nbCompte, Personne(nom), montantInitial, tauxInteret);
		break;
	default:
		break;
	}

	std::cout << std::endl << "Compte cr�� avec succ�s ! Votre num�ro de compte est le " << this->nbCompte << std::endl;

	this->nbCompte++;
}

void Banque::fermerUnCompte(int noCompte, Compte* comptes[])
{
	comptes[noCompte]->etat = false;
	std::cout << "Le compte " << noCompte << " est maintenant d�sactiv�." << std::endl;
}

void Banque::afficherEtatDesComptes(Compte * comptes[], unsigned int & nbCompte)
{
	std::cout << "Etat des comptes (" << nbCompte << " comptes trouv�s dans le systeme)" << std::endl;
	for (int i = 0; i < nbCompte; i++) {
		std::cout << comptes[i]->getNoCompte() << " - " << (comptes[i]->etat ? "Actif" : "Inactif") << std::endl;
	}
}

void Banque::effectuerOperation(Compte * comptes[], unsigned int & nbCompte)
{
	std::cout << "Saisissez votre num�ro de compte --> ";
	int noCompte;
	Valider::Int(noCompte);
	while (noCompte < 0 || noCompte > nbCompte - 1) {
		std::cout << "Compte inexistant, veuillez saisie votre num�ro de compte --> ";
		Valider::Int(noCompte);
	}

	if (comptes[noCompte]->etat) {
		std::cout << "Que voulez-vous faire ?" << std::endl;
		std::cout << "(" << (int)TypeOperation::RETRAIT << ") - RETRAIT" << std::endl;
		std::cout << "(" << (int)TypeOperation::DEPOT << ") - DEPOT" << std::endl;
		std::cout << "--> ";
		int choix;
		Valider::IntEntreBorne(choix, (int)TypeOperation::RETRAIT, (int)TypeOperation::DEPOT);

		std::cout << "Quel montant d�sirez-vous " << (choix == 0 ? "retirer" : "deposer") << " ? --> ";
		int montant;
		Valider::IntPositif(montant);

		switch (choix)
		{
		case (int)TypeOperation::RETRAIT:
			comptes[noCompte]->retrait(montant);
			break;
		case (int)TypeOperation::DEPOT:
			comptes[noCompte]->depot(montant);
			break;
		default:
			break;
		}
	}
	else {
		std::cout << "Le compte " << noCompte << " est d�sactiv�, aucune op�ration ne peut etre effectuer." << std::endl;
	}
}


Banque::~Banque()
{
	for (int i = 0; i < this->nbCompte; i++) {
		delete this->comptes[i];
	}
}
