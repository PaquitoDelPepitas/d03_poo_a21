#include "CompteEpargne.h"
#include <iostream>

CompteEpargne::CompteEpargne(int noCompte, Personne titulaire, int solde, double tauxInteret) : Compte(noCompte, titulaire, solde, tauxInteret, TypeCompte::EPARGNE), frais(10)
{
}

double CompteEpargne::getFrais() const
{
	return CompteEpargne::frais;
}

double CompteEpargne::getTauxInteret() const
{
	return Compte::getTauxInteret() * 2;
}

void CompteEpargne::retrait(int montant)
{
	if (this->getSolde() - montant <= 0) {
		std::cout << "Solde insuffisant pour r�aliser cette transaction. Votre solde est de " << this->getSolde() << "$" << std::endl;
	}
	else {
		this->solde -= montant + CompteEpargne::getFrais();
		std::cout << "Retrait effectu�, des frais de retrait d'un montant de " << CompteEpargne::getFrais() << "$ on �t� ajout�s a l'op�ration." << std::endl;
		std::cout << "Votre solde est d�sormais de " << this->getSolde() << "$" << std::endl;
	}
}

void CompteEpargne::afficher() const
{
	Compte::afficher();
	std::cout << "Type de compte : Epargne" << std::endl;
	std::cout << "Frais de retrait : " << CompteEpargne::getFrais() << "$" << std::endl;
}
