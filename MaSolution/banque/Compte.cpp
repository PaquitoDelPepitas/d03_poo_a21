#include "Compte.h"
#include <iostream>

Compte::Compte(int noCompte, Personne titulaire, int solde, double tauxInteret, TypeCompte type) : noCompte(noCompte), titulaire(titulaire), solde(solde), tauxInteret(tauxInteret), etat(true), type(type)
{
}

void Compte::setNoCompte(int noCompte)
{
	this->noCompte = noCompte;
}

int Compte::getNoCompte() const
{
	return this->noCompte;
}

void Compte::setTitulaire(Personne titulaire)
{
	this->titulaire = titulaire;
}

Personne Compte::getTitulaire() const
{
	return this->titulaire;
}

void Compte::setSolde(int solde)
{
	this->solde = solde;
}

int Compte::getSolde() const
{
	return this->solde;
}

void Compte::setTauxInteret(double tauxInteret)
{
	this->tauxInteret = tauxInteret;
}

double Compte::getTauxInteret() const
{
	return this->tauxInteret;
}

TypeCompte Compte::getType() const
{
	return this->type;
}

void Compte::afficher() const
{
	if (this->etat) {
		std::cout << "Titulaire du compte : ";
		this->getTitulaire().afficher();
		std::cout << std::endl;

		std::cout << "No de compte : " << this->getNoCompte() << std::endl;
		std::cout << "Solde : " << this->getSolde() << "$" << std::endl;
		std::cout << "Taux d'interet : " << this->getTauxInteret() << std::endl;
		std::cout << "Etat : Actif" << std::endl;
	}
	else {
		std::cout << "Etat : Inactif" << std::endl;
	}
}

void Compte::retrait(int montant)
{
	if (this->getSolde() - montant <= 0) {
		std::cout << "Solde insuffisant pour r�aliser cette transaction. Votre solde est de " << this->getSolde() << "$" << std::endl;
	}
	else {
		this->solde -= montant;
		std::cout << "Retrait effectu�, votre solde est d�sormais de " << this->getSolde() << "$" << std::endl;
	}
}

void Compte::depot(int montant)
{
	this->solde += montant;
	std::cout << "D�pot effectu�. Votre solde est d�sormais de " << this->getSolde() << "$" << std::endl;
}
