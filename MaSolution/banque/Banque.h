#pragma once
#include "Compte.h"
#include "Personne.h"
#include "validations.h"
#include "TypeOperation.h"
#include "CompteCheque.h"
#include "CompteEpargne.h"

class Banque
{
	std::string nom;
	Personne directeur;

public:
	Compte* comptes[50];
	unsigned int nbCompte;

	Banque();

	void ouvrirUnCompte(Compte* comptes[], unsigned int & nbCompte);
	void fermerUnCompte(int noCompte, Compte* comptes[]);

	void afficherEtatDesComptes(Compte* comptes[], unsigned int & nbCompte);

	void effectuerOperation(Compte* comptes[], unsigned int & nbCompte);

	~Banque();
};

