#include "TableauDynamique.h"
#include <iostream>

TableauDynamique::TableauDynamique()
{
	this->nbElem = 0;
	this->maxElem = 1;
	this->tab = new double[maxElem];
}

void TableauDynamique::ajouter(double val)
{
	if (this->nbElem < this->maxElem) {
		this->tab[this->nbElem] = val;
		this->nbElem++;
	}
	else {
		double *newTab;
		newTab = new double[maxElem * 2];

		for (int i = 0; i < this->nbElem; i++) {
			newTab[i] = this->tab[i];
		}
		
		delete this->tab;
		this->tab = newTab;
		this->maxElem *= 2;
		this->tab[this->nbElem] = val;
		this->nbElem++;
	}
}

void TableauDynamique::afficher() const
{
	std::cout << "Maximum d'�l�ments : " << this->maxElem << std::endl;
	for (int i = 0; i < this->nbElem; i++) {
		std::cout << i + 1 << "e �l�ment : " << this->tab[i] << std::endl;
	}
}

TableauDynamique::~TableauDynamique()
{
	for (int i = 0; i < this->nbElem; i++) {
		delete &this->tab[i];
	}
}
