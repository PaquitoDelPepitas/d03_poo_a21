#pragma once
class TableauDynamique
{
	int nbElem, maxElem;
	double* tab;

public:
	TableauDynamique();
	void ajouter(double val);
	void afficher() const;
	~TableauDynamique();
};

