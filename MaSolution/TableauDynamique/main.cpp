#include <iostream>
using namespace std;

#include "TableauDynamique.h"

int main() {
	setlocale(LC_ALL, "");

	TableauDynamique* tab = new TableauDynamique();
	tab->ajouter(1.111);
	tab->ajouter(2.222);
	tab->ajouter(3.333);
	tab->ajouter(4.444);
	tab->ajouter(5.555);
	tab->afficher();
	delete tab;

	system("pause");
	return 0;
}