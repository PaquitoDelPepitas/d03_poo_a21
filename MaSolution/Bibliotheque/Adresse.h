#pragma once
class Adresse
{
	unsigned short numeroRue;
	char nomRue[61], ville[41], pays[41], codePostal[16];

public:
	Adresse(unsigned short numeroRue, const char nomRue[], const char ville[], const char pays[], const char codePostal[]);

	void afficher();

	~Adresse();
};

