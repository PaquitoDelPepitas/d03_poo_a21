#pragma once

#include "Adresse.h"

class Editeur
{
	char nom[76];
	Adresse adresse;

public:
	Editeur();
	Editeur(const char nom[], Adresse adresse);

	void setNom(const char nom[]);
	const char* getNom();

	void setAdresse(Adresse adresse);
	Adresse getAdresse();

	void afficher();

	~Editeur();
};

