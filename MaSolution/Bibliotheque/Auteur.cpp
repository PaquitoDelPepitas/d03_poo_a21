#include "Auteur.h"
#include "validations.h"
#include <iostream>


Auteur::Auteur()
{
	strcpy_s(Auteur::nom, "N/D");
	strcpy_s(Auteur::prenom, "N/D");
	Auteur::contribution = 0;
}

Auteur::Auteur(const char nom[], const char prenom[], unsigned short contribution)
{
	Auteur::setNom(nom);
	Auteur::setPrenom(prenom);
	Auteur::setContribution(contribution);
}

void Auteur::setNom(const char nom[])
{
	strcpy_s(Auteur::nom, nom);
}

const char * Auteur::getNom()
{
	return Auteur::nom;
}

void Auteur::setPrenom(const char prenom[])
{
	strcpy_s(Auteur::prenom, prenom);
}

const char * Auteur::getPrenom()
{
	return Auteur::prenom;
}

void Auteur::setContribution(unsigned short contribution)
{
	Auteur::contribution = (contribution > 100 ? 100 : contribution);
	Auteur::contribution = (Auteur::contribution < 0 ? 0 : Auteur::contribution);
}

unsigned short Auteur::getConstribution()
{
	return Auteur::contribution;
}

void Auteur::afficher()
{
	std::cout << Auteur::getNom() << " " << Auteur::getPrenom() << ", " << Auteur::getConstribution() << "%";
}


Auteur::~Auteur()
{
}
