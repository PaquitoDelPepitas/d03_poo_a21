#pragma once

#include "Editeur.h"
#include "Auteur.h"

class Livre
{
	static unsigned int nbLivres;

	unsigned long long identifiantSysteme;
	short anneeParution;
	unsigned short nbPages;

	static const unsigned short MAX_CHAR_TITRE = 200;
	static const unsigned short MAX_CHAR_ISBN = 14;
	char titre[MAX_CHAR_TITRE + 1], ISBN13[MAX_CHAR_ISBN + 1];

	Editeur editeur;

	Auteur contributeur[100];
	unsigned short nbAuteur;

public:
	Livre();
	Livre(unsigned long long identifiantSysteme, short anneeParution, unsigned short nbPages, const char titre[], const char ISNB13[], Editeur editeur);
	Livre(unsigned long long identifiantSysteme, short anneeParution, unsigned short nbPages, const char titre[], const char ISBN13[], Editeur editeur, Auteur contributeur[], unsigned short nbAuteur);

	void setIdentifiantSysteme(unsigned long long identifiantSysteme);
	unsigned long long getIdentifiantSysteme();

	void setAnneeParution(short anneeParution);
	short getAnneeParution();

	void setNbPages(unsigned short nbPages);
	unsigned short getNbPages();

	void setTitre(const char titre[]);
	const char* getTitre();

	void setISBN13(const char ISBN13[]);
	const char* getISBN13();

	void setEditeur(Editeur editeur);
	Editeur getEditeur();

	Auteur getContributeur();
	unsigned short getNbAuteur();

	unsigned short recupererSommeContribution(Auteur contributeur[], unsigned short nbAuteur);

	void ajouterContributeur();
	void retirerContributeur();

	static Livre creerLivre();

	void afficher();
	void afficherContributeurs();

	~Livre();
};

