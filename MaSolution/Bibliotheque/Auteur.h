#pragma once
class Auteur
{
	char nom[51], prenom[51];
	unsigned short contribution;

public:
	Auteur();
	Auteur(const char nom[], const char prenom[], unsigned short contribution);

	void setNom(const char nom[]);
	const char* getNom();

	void setPrenom(const char prenom[]);
	const char* getPrenom();

	void setContribution(unsigned short contribution);
	unsigned short getConstribution();

	void afficher();

	~Auteur();
};

