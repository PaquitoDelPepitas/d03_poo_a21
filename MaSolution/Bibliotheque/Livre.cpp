#include "Livre.h"
#include <iostream>

unsigned int Livre::nbLivres = 0;

void Livre::setIdentifiantSysteme(unsigned long long identifiantSysteme)
{
	Livre::identifiantSysteme = identifiantSysteme;
}

unsigned long long Livre::getIdentifiantSysteme()
{
	return Livre::identifiantSysteme;
}

void Livre::setAnneeParution(short anneeParution)
{
	Livre::anneeParution = anneeParution;
}

short Livre::getAnneeParution()
{
	return Livre::anneeParution;
}

void Livre::setNbPages(unsigned short nbPages)
{
	Livre::nbPages = nbPages;
}

unsigned short Livre::getNbPages()
{
	return Livre::nbPages;
}

void Livre::setTitre(const char titre[])
{
	strcpy_s(Livre::titre, titre);
}

const char * Livre::getTitre()
{
	return Livre::titre;
}

void Livre::setISBN13(const char ISBN13[])
{
	strcpy_s(Livre::ISBN13, ISBN13);
}

const char * Livre::getISBN13()
{
	return Livre::ISBN13;
}

void Livre::setEditeur(Editeur editeur)
{
	// Livre::editeur.setNom(editeur.getNom());
	Livre::editeur = editeur;
}

Editeur Livre::getEditeur()
{
	return Livre::editeur;
}

Auteur Livre::getContributeur()
{
	return Livre::contributeur[Livre::nbAuteur];
}

unsigned short Livre::getNbAuteur()
{
	return Livre::nbAuteur;
}

unsigned short Livre::recupererSommeContribution(Auteur contributeur[], unsigned short nbAuteur)
{
	unsigned short somme = 0;
	for (int i = 0; i < nbAuteur; i++) {
		somme += Livre::contributeur[i].getConstribution();
	}
	return somme;
}

void Livre::ajouterContributeur()
{
	Auteur* contributeur = new Auteur;
	std::cout << "Nom du contributeur --> ";
	char nom[51];
	std::cin.ignore(512, '\n');
	std::cin.getline(nom, 51);
	contributeur->setNom(nom);

	std::cout << "Pr�nom du contributeur --> ";
	char prenom[51];
	std::cin.getline(prenom, 51);
	contributeur->setPrenom(prenom);

	std::cout << "Part de contribution de l'auteur --> ";
	unsigned short part, somme = 0;

	while(somme > 100 || somme == 0){
		std::cout << (somme > 100 ? "Attention, le total des contribution ne peut depasser 100%. veuillez saisir une nouvelle valeur --> " : "");
		std::cin >> part;
		somme = Livre::recupererSommeContribution(Livre::contributeur, Livre::nbAuteur) + part;
	}
	contributeur->setContribution(part);

	Livre::contributeur[Livre::nbAuteur] = *contributeur;
	Livre::nbAuteur++;

	delete contributeur;
}

void Livre::retirerContributeur()
{
	Livre::afficherContributeurs();
	std::cout << std::endl << "Saisir le num�ro de l'auteur a retirer --> ";

	int indice;
	std::cin >> indice;

	Livre::contributeur[indice] = Livre::contributeur[Livre::nbAuteur - 1];
	Livre::nbAuteur--;
}

Livre Livre::creerLivre()
{
	Livre* livre = new Livre;

	std::cout << std::endl;
	std::cout << "NOUVEAU LIVRE" << std::endl;
	
	std::cout << "Identifiant systeme --> ";
	std::cin >> livre->identifiantSysteme;

	std::cin.ignore(512, '\n');

	std::cout << "Titre --> ";
	std::cin.getline(livre->titre, livre->MAX_CHAR_TITRE);

	std::cout << "ISBN13 --> ";
	std::cin.getline(livre->ISBN13, livre->MAX_CHAR_ISBN);

	std::cout << "Annee de parution --> ";
	std::cin >> livre->anneeParution;

	std::cout << "Nombre de page --> ";
	std::cin >> livre->nbPages;

	std::cout << std::endl;

	Editeur* newEditeur = new Editeur;
	livre->editeur = *newEditeur;
	delete newEditeur;

	livre->nbAuteur = 0;

	return *livre;
}

void Livre::afficher()
{
	std::cout << "Identifiant syst�me : " << Livre::getIdentifiantSysteme() << std::endl;
	std::cout << "Titre : " << Livre::getTitre() << std::endl;
	std::cout << "ISBN13 : " << Livre::getISBN13() << std::endl;
	std::cout << "Ann�e de parution : " << Livre::getAnneeParution() << std::endl;
	std::cout << "Nombre de page : " << Livre::getNbPages() << std::endl;
	std::cout << std::endl;

	Livre::editeur.afficher();
	std::cout << std::endl;
	std::cout << std::endl;

	Livre::afficherContributeurs();
	std::cout << std::endl;

	std::cout << "Total de contribution des auteurs : " << Livre::recupererSommeContribution(Livre::contributeur, Livre::nbAuteur) << "%" << std::endl;
	std::cout << std::endl;
}

void Livre::afficherContributeurs()
{
	std::cout << "Auteurs : " << std::endl;
	for (int i = 0; i < Livre::nbAuteur; i++) {
		std::cout << i << " - ";
		Livre::contributeur[i].afficher(); 
		std::cout << std::endl;
	}
}


Livre::Livre() : editeur()
{
	Livre::setIdentifiantSysteme(0);
	Livre::setAnneeParution(0);
	Livre::setNbPages(0);
	strcpy_s(Livre::titre, "N/D");
	strcpy_s(Livre::ISBN13, "N/D");
	Livre::nbAuteur = 0;
	Livre::nbLivres++;
}

Livre::Livre(unsigned long long identifiantSysteme, short anneeParution, unsigned short nbPages, const char titre[], const char ISBN13[], Editeur editeur) : editeur(editeur)
{
	Livre::setIdentifiantSysteme(identifiantSysteme);
	Livre::setAnneeParution(anneeParution);
	Livre::setNbPages(nbPages);
	strcpy_s(Livre::titre, titre);
	strcpy_s(Livre::ISBN13, ISBN13);
	// Livre::editeur = editeur;
	// Inutile car renseign� avec ": editeur(editeur)"
	Livre::nbAuteur = 0;
	Livre::nbLivres++;
}

Livre::Livre(unsigned long long identifiantSysteme, short anneeParution, unsigned short nbPages, const char titre[], const char ISBN13[], Editeur editeur, Auteur contributeur[], unsigned short nbAuteur) : editeur(editeur), nbAuteur(nbAuteur)
{
	Livre::setIdentifiantSysteme(identifiantSysteme);
	Livre::setAnneeParution(anneeParution);
	Livre::setNbPages(nbPages);
	strcpy_s(Livre::titre, titre);
	strcpy_s(Livre::ISBN13, ISBN13);

	for (int i = 0; i < nbAuteur; i++) {
		Livre::contributeur[i] = contributeur[i];
	}
}


Livre::~Livre()
{
}