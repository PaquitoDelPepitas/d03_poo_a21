#include <iostream>
#include "Livre.h"
#include "Auteur.h"
#include "Editeur.h"
#include "Adresse.h"
#include "Tablette.h"
#include <string>

using namespace std;

// void creerLivre();

// Livre newLivre;

int main() {
	setlocale(LC_ALL, "");

	/*Livre* livre1 = new Livre(Livre::creerLivre());
	Livre* livre2 = new Livre(Livre::creerLivre());*/

	Livre* tabLivres[1] = {} ;

	Tablette* tablette = new Tablette(tabLivres, 0, "Pepito");

	cout << endl;
	cout << "**********" << endl;
	cout << endl;

	tablette->afficherInfoTablette();

	tablette->ajouterLivre();

	tablette->afficherInfoTablette();

	tablette->retirerLivre();

	tablette->afficherInfoTablette();

	delete tablette;


	// delete tabLivres, livre1, livre2;

	/*
	Auteur auteur1("Doe", "John", 25), auteur2("B", "Ben", 50);
	Auteur contributeur[2] = { auteur1, auteur2 };
	Livre* livre = new Livre(5004, 2021, 861, "Super Titre", "000-123456", Editeur("BenjiBook", Adresse(19, "Rue Saint-Pierre", "Sainte-Therese", "Canada", "J7E 2S4")), contributeur, 2);

	livre->afficher();
	*/

	/*
	int reponse;

	do {
		cout << "Que faire ?" << endl;
		if (!(livre->recupererSommeContribution(&livre->getContributeur(), livre->getNbAuteur()) == 100)) {
			cout << "(1) - Ajouter contributeur" << endl;
		}
		if (livre->getNbAuteur() > 0) {
			cout << "(2) - Retirer contributeur" << endl;
		}
		cout << "(3) - Ajouter un livre" << endl;
		cout << "(4) - Arreter" << endl;
		cout << "--> ";
		cin >> reponse; 

		switch (reponse) {
			case 1:
				livre->ajouterContributeur();
				livre->afficher();
				break;
			case 2:
				livre->retirerContributeur();
				livre->afficher();
				break;
			case 3:
				creerLivre();
				newLivre.afficher();
				break;
			default:
				break;
		}

	} while (reponse != 4);
	*/

	// delete livre;

	/*
	Auteur* auteur = new Auteur();
	auteur->afficher();
	delete auteur;
	*/

	/* 
	Editeur* editeur = new Editeur();
	editeur->afficher();
	delete editeur;
	*/

	/* 
	Adresse* adresse = new Adresse(212, "Rue Saint-Pierre", "Sainte-Therese", "Canada", "J7E 2S4");
	adresse->afficher();
	delete adresse;
	*/

	system("pause");
	return 0;
}

/*
void creerLivre()
{
	newLivre = Livre::creerLivre();
}
*/
