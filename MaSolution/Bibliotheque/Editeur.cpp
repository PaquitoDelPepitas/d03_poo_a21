#include "Editeur.h"
#include <iostream>


Editeur::Editeur() : adresse(0, "N/D", "N/D", "N/D", "N/D")
{
	Editeur::setNom("N/D");
}

Editeur::Editeur(const char nom[], Adresse adresse) : adresse(adresse)
{
	Editeur::setNom(nom);
}

void Editeur::setNom(const char nom[])
{
	strcpy_s(Editeur::nom, nom);
}

const char * Editeur::getNom()
{
	return Editeur::nom;
}

void Editeur::setAdresse(Adresse adresse)
{
	Editeur::adresse = adresse;
}

Adresse Editeur::getAdresse()
{
	return Editeur::adresse;
}

void Editeur::afficher()
{
	std::cout << "Les �ditions " << Editeur::getNom() << std::endl;
	Editeur::adresse.afficher();
}


Editeur::~Editeur()
{
}
