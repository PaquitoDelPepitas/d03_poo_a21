#include "validations.h"
#include <iostream>

// int
static void Int(int & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
static void IntEntreBorne(int & aValider, int min, int max)
{
	Valider::Int(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::Int(aValider);
	}
}
static void IntPositif(int & aValider)
{
	Valider::Int(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::Int(aValider);
	}
}

// short
static void Short(short & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
static void ShortEntreBorne(short & aValider, int min, int max)
{
	Valider::Short(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::Short(aValider);
	}
}
static void ShortPositif(short & aValider)
{
	Valider::Short(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::Short(aValider);
	}
}

// unsigned short
static void UnsignedShort(unsigned short & aValider)
{
	int valeur;
	std::cin >> valeur;
	while (std::cin.fail() || std::cin.peek() != '\n' || valeur < 0 || valeur > USHRT_MAX) {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> valeur;
	}
	std::cin.ignore(512, '\n');
	aValider = valeur;
}
static void UnsignedShortEntreBorne(unsigned short & aValider, int min, int max)
{
	Valider::UnsignedShort(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::UnsignedShort(aValider);
	}
}

// float
static void Float(float & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
static void FloatEntreBorne(float & aValider, int min, int max)
{
	Valider::Float(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::Float(aValider);
	}
}
static void FloatPositif(float & aValider)
{
	Valider::Float(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::Float(aValider);
	}
}

// double
static void Double(double & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
static void DoubleEntreBorne(double & aValider, int min, int max)
{
	Valider::Double(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::Double(aValider);
	}
}
static void DoublePositif(double & aValider)
{
	Valider::Double(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::Double(aValider);
	}
}

// long double
static void LongDouble(long double & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
static void LongDoubleEntreBorne(long double & aValider, int min, int max)
{
	Valider::LongDouble(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::LongDouble(aValider);
	}
}
static void LongDoublePositif(long double & aValider)
{
	Valider::LongDouble(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::LongDouble(aValider);
	}
}

// long long
static void LongLong(long long & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
static void LongLongEntreBorne(long long & aValider, int min, int max)
{
	Valider::LongLong(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::LongLong(aValider);
	}
}
static void LongLongPositif(long long & aValider)
{
	Valider::LongLong(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::LongLong(aValider);
	}
}

// char
static void Char(char & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Veuillez saisir un caract�re valide --> ";
		std::cin >> aValider;
	}
}
static void CharComparativement(char & aValider, char ref1, char ref2)
{
	Valider::Char(aValider);
	while (toupper(aValider) != ref1 && toupper(aValider) != ref2){
		std::cout << "ERREUR - Veuillez saisir " << ref1 << " ou " << ref2 << " --> ";
		Valider::Char(aValider);
	}
}

 // chaine
static void Chaine(char chaine[], int taille)
{
	std::cin.getline(chaine, taille);
	while (std::cin.fail()) {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Saisie trop longue, efefctuez une saisie plus courte --> ";
		std::cin.getline(chaine, taille);
	}
}