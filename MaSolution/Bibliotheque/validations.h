#pragma once

class Valider {
public: 
	// int 
	static void Int(int &aValider);
	static void IntEntreBornes(int &aValider, int min, int max);
	static void IntPositif(int &aValider);

	// short
	static void Short(short &aValider);
	static void ShortEntreBorne(short &aValider, int min, int max);
	static void ShortPositif(short &aValider);

	// unsigned short
	static void UnsignedShort(unsigned short &aValider);
	static void UnsignedShortEntreBorne(unsigned short &aValider, int min, int max);

	// float
	static void Float(float &aValider);
	static void FloatEntreBorne(float &aValider, int min, int max);
	static void FloatPositif(float &aValider);

	// double 
	static void Double(double &aValider);
	static void DoubleEntreBorne(double &aValider, int min, int max);
	static void DoublePositif(double &aValider);

	// long double 
	static void LongDouble(long double &aValider);
	static void LongDoubleEntreBorne(long double &aValider, int min, int max);
	static void LongDoublePositif(long double &aValider);

	// long long 
	static void LongLong(long long &aValider);
	static void LongLongEntreBorne(long long &aValider, int min, int max);
	static void LongLongPositif(long long &aValider);

	// char
	static void Char(char &aValider);
	static void CharComparativement(char &aValider, char ref1, char ref2);

	// chaine
	static void Chaine(char chaine[], int taille);
};