#include "Adresse.h"
#include <iostream>




Adresse::Adresse(unsigned short numeroRue, const char nomRue[], const char ville[], const char pays[], const char codePostal[])
{
	Adresse::numeroRue = numeroRue;
	strcpy_s(Adresse::nomRue, nomRue);
	strcpy_s(Adresse::ville, ville);
	strcpy_s(Adresse::pays, pays);
	strcpy_s(Adresse::codePostal, codePostal);
}

void Adresse::afficher()
{
	std::cout << Adresse::numeroRue << " " << Adresse::nomRue << std::endl;
	std::cout << Adresse::ville << ", " << Adresse::pays << ", " << Adresse::codePostal;
}

Adresse::~Adresse()
{
}
