#include "Tablette.h"
#include <iostream>
#include <string>
#include "validations.h"


Tablette::Tablette()
{
	this->nbLivre = 0;
	*this->livres = { 0 };
	this->codeDeTablette = "";
}

Tablette::Tablette(Livre * livres[], short nbLivre, std::string code) : nbLivre(nbLivre), codeDeTablette(code)
{
	for (int i = 0; i < this->nbLivre; i++) {
		this->livres[i] = new Livre(*livres[i]);
	}
}

void Tablette::afficher() const
{
	for (int i = 0; i < this->nbLivre; i++) {
		this->livres[i]->afficher();
	}
}

void Tablette::ajouterLivre()
{
	if (this->nbLivre < 25) {
		this->livres[this->nbLivre] = new Livre(Livre::creerLivre());
		this->nbLivre++;
	}
	else {
		std::cout << "La capacit� maximale de 25 livres est atteinte pour cette tablette, veuillez retirer un livre si vous voulez en ajouter un nouveau." << std::endl;
	}
}

void Tablette::setCodeDeTablette(std::string codeDeTablette)
{
	this->codeDeTablette = codeDeTablette;
}

std::string Tablette::getCodeDeTablette() const
{
	return this->codeDeTablette;
}

short Tablette::chercherLivre(int id)
{
	short reponse = -1;


	short cpt = 0;

	while (cpt < this->nbLivre && reponse == -1) {
		if (this->livres[cpt]->getIdentifiantSysteme() == id) {
			reponse = cpt;
		}
		cpt++;
	};

	return reponse;
}

void Tablette::retirerLivre()
{
	std::cout << "Saisir l'identifiant du livre a retirer --> ";
	int id;
	Valider::Int(id);

	int aRetirer = this->chercherLivre(id);

	if (aRetirer != -1) {
		delete this->livres[aRetirer];
		//if (aRetirer != this->nbLivre - 1) {
			this->livres[aRetirer] = this->livres[nbLivre - 1];
		//}
		this->nbLivre--;
	}
	else {
		std::cout << "Aucun livre ne correspond a cet ID" << std::endl;
	}
}

void Tablette::afficherInfoTablette() const
{
	std::cout << "Code de la tablette : " << this->codeDeTablette << std::endl;
	this->afficher();
}


Tablette::~Tablette()
{
	for (int i = 0; i < this->nbLivre; i++) {
		delete this->livres[i];
	}
}
