#pragma once
#include "Livre.h"
#include <iostream>
#include <string>

class Tablette
{
	Livre* livres[25] = {};
	short nbLivre;
	std::string codeDeTablette;

public:
	Tablette();
	Tablette(Livre* livres[], short nbLivre, std::string code);
	void afficher() const;
	void ajouterLivre();

	void setCodeDeTablette(std::string codeDeTablette);
	std::string getCodeDeTablette() const;

	short chercherLivre(int id);
	void retirerLivre();

	void afficherInfoTablette() const;

	~Tablette();
};

