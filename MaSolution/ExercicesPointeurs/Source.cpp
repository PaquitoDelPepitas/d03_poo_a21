#include <iostream>
using namespace std;

int main() {

	double x = 123.456;
	double * ptr_x = &x;

	cout << x << endl;

	*ptr_x = 3.14159;

	cout << x << endl;

	return 0;
}