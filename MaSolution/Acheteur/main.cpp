#include <iostream>
using namespace std; 

#include "Date.h"
#include "Adresse.h"
#include "Achats.h"
#include "Acheteur.h"

int main() {
	setlocale(LC_ALL, "");

	Date* dateVente1 = new Date(2, 8, 2000);
	Date* dateVente2 = new Date(4, 7, 2003);
	//dateVente->afficher();
	//cout << endl;

	Adresse* adresse = new Adresse(212, "Rue Saint-Pierre", "Sainte-Th�r�se", "J7E 2S4");
	//adresse->afficher();
	//cout << endl;

	Achats* achat1 = new Achats(*dateVente1, 3500);
	Achats* achat2 = new Achats(*dateVente2, 4800);
	//achat1->afficher();
	//cout << endl;

	Achats tabAchat[2] = { *achat1, *achat2 };

	Acheteur* acheteur = new Acheteur(1, 20, *adresse, 5000, tabAchat, 2);
	acheteur->afficher();
	cout << endl;


	delete dateVente1;
	delete dateVente2;
	delete adresse;
	delete achat1;
	delete achat2;
	delete acheteur;

	system("pause");
	return 0;
}