#include "Acheteur.h"
#include <iostream>
#include "Achats.h"

Acheteur::Acheteur(int numero, unsigned short age, Adresse adresse, int solde, Achats achat[], unsigned int nbAchats) : numero(numero), age(age), adresse(adresse), solde(solde), nbAchats(nbAchats)
{
	for (int i = 0; i < nbAchats; i++) {
		this->achat[i] = &achat[i];
	}
}

void Acheteur::afficher() const
{
	std::cout << "Numero de l'acheteur : " << this->numero << std::endl;
	std::cout << "Age : " << this->age << std::endl;
	std::cout << "Adresse : ";
	this->adresse.afficher();
	std::cout << std::endl;
	std::cout << "Solde : " << this->solde << std::endl;
	std::cout << "Nombre d'achats : " << this->nbAchats << std::endl;

	for (int i = 0; i < nbAchats; i++) {
		this->achat[i]->afficher();
		std::cout << std::endl;
	}
}


