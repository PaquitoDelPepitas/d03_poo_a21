#include "Adresse.h"
#include <iostream>

Adresse::Adresse(unsigned short numero, const char rue[], const char ville[], const char cp[]) : numero(numero)
{
	strcpy_s(this->rue, rue);
	strcpy_s(this->ville, ville);
	strcpy_s(this->cp, cp);
}

void Adresse::afficher() const
{
	std::cout << this->numero << " " << this->rue << ", " << this->ville << ", " << this->cp;
}
