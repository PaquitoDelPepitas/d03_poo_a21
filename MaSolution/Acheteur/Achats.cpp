#include "Achats.h"
#include <iostream>

Achats::Achats(Date dateVente, int montant) : dateVente(dateVente), montant(montant)
{
}

void Achats::afficher() const
{
	std::cout << "Achat de " << this->montant << "$ effectu� le ";
	dateVente.afficher();
}
