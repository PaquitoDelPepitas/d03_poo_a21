#pragma once
#include <string>
class Adresse
{
	unsigned short numero;
	char rue[81], ville[51], cp[8];

public:
	Adresse(unsigned short numero, const char rue[], const char ville[], const char cp[]);
	void afficher() const;
};

