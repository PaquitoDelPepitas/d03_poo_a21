#include "Date.h"
#include <iostream>

Date::Date(unsigned short jour, unsigned short mois, unsigned short annee) : jour(jour), mois(mois), annee(annee)
{
}

void Date::afficher() const
{
	std::cout << (this->jour < 10 ? "0" : "") << this->jour << "/" << (this->mois < 10 ? "0" : "") << this->mois << "/" << this->annee;
}
