#pragma once
#include "Adresse.h"
#include "Achats.h"

class Acheteur
{
	int numero;
	unsigned short age;
	Adresse adresse;
	int solde;
	Achats* achat[100];
	unsigned int nbAchats;

public:

	Acheteur(int numero, unsigned short age, Adresse adresse, int solde, Achats achat[], unsigned int nbAchats);
	void afficher() const;
};

