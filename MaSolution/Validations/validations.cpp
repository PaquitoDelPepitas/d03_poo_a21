#include "validations.h"
#include <iostream>

// int
void Valider::Int(int & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
void Valider::IntEntreBorne(int & aValider, int min, int max)
{
	Valider::Int(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::Int(aValider);
	}
}
void Valider::IntPositif(int & aValider)
{
	Valider::Int(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::Int(aValider);
	}
}

// short
void Valider::Short(short & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
void Valider::ShortEntreBorne(short & aValider, int min, int max)
{
	Valider::Short(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::Short(aValider);
	}
}
void Valider::ShortPositif(short & aValider)
{
	Valider::Short(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::Short(aValider);
	}
}

// unsigned short
void Valider::UnsignedShort(unsigned short & aValider)
{
	int valeur;
	std::cin >> valeur;
	while (std::cin.fail() || std::cin.peek() != '\n' || valeur < 0 || valeur > USHRT_MAX) {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> valeur;
	}
	std::cin.ignore(512, '\n');
	aValider = valeur;
}
void Valider::UnsignedShortEntreBorne(unsigned short & aValider, int min, int max)
{
	Valider::UnsignedShort(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::UnsignedShort(aValider);
	}
}

// float
void Valider::Float(float & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
void Valider::FloatEntreBorne(float & aValider, int min, int max)
{
	Valider::Float(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::Float(aValider);
	}
}
void Valider::FloatPositif(float & aValider)
{
	Valider::Float(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::Float(aValider);
	}
}

// double
void Valider::Double(double & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
void Valider::DoubleEntreBorne(double & aValider, int min, int max)
{
	Valider::Double(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::Double(aValider);
	}
}
void Valider::DoublePositif(double & aValider)
{
	Valider::Double(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::Double(aValider);
	}
}

// long double
void Valider::LongDouble(long double & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
void Valider::LongDoubleEntreBorne(long double & aValider, int min, int max)
{
	Valider::LongDouble(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::LongDouble(aValider);
	}
}
void Valider::LongDoublePositif(long double & aValider)
{
	Valider::LongDouble(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::LongDouble(aValider);
	}
}

// long long
void Valider::LongLong(long long & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Valeur invalide. Rentrez une autre valeur --> ";
		std::cin >> aValider;
	}
	std::cin.ignore(512, '\n');
}
void Valider::LongLongEntreBorne(long long & aValider, int min, int max)
{
	Valider::LongLong(aValider);
	while (!(aValider >= min && aValider <= max)) {
		std::cout << "ERREUR - Valeur entre " << min << " et " << max << " attendue --> ";
		Valider::LongLong(aValider);
	}
}
void Valider::LongLongPositif(long long & aValider)
{
	Valider::LongLong(aValider);
	while (!(aValider >= 0)) {
		std::cout << "ERREUR - Valeur positive attendue --> ";
		Valider::LongLong(aValider);
	}
}

// char
void Valider::Char(char & aValider)
{
	std::cin >> aValider;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Veuillez saisir un caract�re valide --> ";
		std::cin >> aValider;
	}
}
void Valider::CharComparativement(char & aValider, char ref1, char ref2)
{
	Valider::Char(aValider);
	while (toupper(aValider) != ref1 && toupper(aValider) != ref2){
		std::cout << "ERREUR - Veuillez saisir " << ref1 << " ou " << ref2 << " --> ";
		Valider::Char(aValider);
	}
}

 // chaine
void Valider::Chaine(char chaine[], int taille)
{
	std::cin.getline(chaine, taille);
	while (std::cin.fail()) {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "ERREUR - Saisie trop longue, efefctuez une saisie plus courte --> ";
		std::cin.getline(chaine, taille);
	}
}