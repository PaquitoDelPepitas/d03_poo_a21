#include "nombres.h"
#include <iostream>

const int MIN = 0, MAX = 100;

int validerNumerique()
{
	int valeur;
	std::cin >> valeur;
	while (std::cin.fail() || std::cin.peek() != '\n') {
		std::cin.clear();
		std::cin.ignore(512, '\n');
		std::cout << "Valeur enti�re seulement : ";
		std::cin >> valeur;
	}
	std::cin.ignore(512, '\n');

	return valeur;
}

int validerPlage(int min, int max)
{
	int valeur = validerNumerique();
	while (!(valeur >= min && valeur <= max)) {
		std::cout << "Valeur entre " << min << " et " << max << " uniquement : ";
		valeur = validerNumerique();
	}

	return valeur;
}

void lire(int* val1, int * val2, int * val3)
{
	std::cout << "Saisir une premi�re valeur : ";
	*val1 = validerPlage(MIN, MAX);

	std::cout << "Saisir une seconde valeur : ";
	*val2 = validerPlage(MIN, MAX);

	std::cout << "Saisir une troisi�me valeur : ";
	*val3 = validerPlage(MIN, MAX);
}

void echanger(int * petit, int * grand)
{
	int save = *petit;
	*petit = *grand;
	*grand = save;
}

void trier(int * val1, int * val2, int * val3)
{
	if (*val1 > *val2) {
		echanger(val2, val1);

		if (*val2 > *val3) {
			echanger(val2, val3);

			if (*val1 > *val2) {
				echanger(val2, val1);
			}
		}
	}
	else if (*val1 > *val3) {
		echanger(val3, val1);

		if (*val2 > *val3) {
			echanger(val3, val2);
		}
	}
	else if (*val2 > *val3) {
		echanger(val3, val2);
	}
}

void afficher(int* val1, int* val2, int* val3)
{
	std::cout << *val1 << " - " << *val2 << " - " << *val3 << std::endl;
}
