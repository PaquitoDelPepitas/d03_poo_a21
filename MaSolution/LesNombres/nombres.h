#pragma once
int validerNumerique();
int validerPlage(int min, int max);
void lire(int* val1, int* val2, int* val3);
void echanger(int* petit, int* grand);
void trier(int* val1, int* val2, int* val3);
void afficher(int* val1, int* val2, int* val3);