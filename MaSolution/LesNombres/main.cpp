#include <iostream>
#include "nombres.h"

using namespace std;

int main() {
	setlocale(LC_ALL, "");

	int val1, val2, val3;
	lire(&val1, &val2, &val3);
	trier(&val1, &val2, &val3);
	afficher(&val1, &val2, &val3);

	system("pause");
	return 0;
}